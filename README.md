# rbm_refs

Classification using Discriminative Restricted Boltzmann Machines
Hugo Larochelle
larocheh@iro.umontreal.ca
Yoshua Bengio
bengioy@iro.umontreal.ca
Dept. IRO, Université de Montréal C.P. 6128, Montreal, Qc, H3C 3J7, Canada

The "Wake-Sleep" Algorithm for
Unsupervised Neural Networks
Geoffrey E. Hinton,* Peter Dayan, Brendan J. Frey,
Radford M. Neal

A modified version of Helmholtz machine by using a Restricted
Boltzmann Machine to model the generative probability of the
top layer
Junying Hu a , Jiangshe Zhang a , ∗ , Nannan Ji b , Chunxia Zhang a
a
b
School of Mathematics and Statistics, Xi’an Jiaotong University, China
Department of Mathematics and Information Science, College of Science, Chang’an University, China

Restricted Boltzmann Machines:
Introduction and Review
Guido Montúfar

A topological insight into restricted Boltzmann machines
Decebal Constantin Mocanu 1 · Elena Mocanu 1 · Phuong H. Nguyen 1 ·
Madeleine Gibescu 1 · Antonio Liotta 1
Received: 3 December 2015 / Accepted: 16 June 2016 / Published online: 15 July 2016
© The Author(s) 2016. This article is published with open access at Springerlink.com

Online Semi-Supervised Learning with Deep Hybrid Boltzmann Machines and Denoising Autoencoders

Online Learning of Deep Hybrid Architectures forSemi-Supervised Categorization

An Infinite Restricted Boltzmann MachineMarc-Alexandre Cˆot ́e1, Hugo Larochelle1

Bidirectional Helmholtz MachinesJ ̈org Bornschein, Samira Shabanian, Asja Fischer, Yoshua Bengio∗Dept. Computer Science and Operations Research, University of Montreal∗Yoshua Bengio is a CIFAR Senior Fellow

Boltzmann Encoded Adversarial Machines